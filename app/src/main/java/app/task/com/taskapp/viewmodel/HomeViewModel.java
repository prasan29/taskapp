package app.task.com.taskapp.viewmodel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

import app.task.com.taskapp.task.CamScannerTask;

@SuppressLint("StaticFieldLeak")
public class HomeViewModel extends ViewModel {
    private static final String EMAIL = "";
    private static final String TAG = "HomeActivity";
    public final int REQ_CODE_PICK_IMAGE = 1;
    public final int REQ_CODE_CALL_CAMSCANNER = 2;
    public MutableLiveData<String> mLoginStatusText = new MutableLiveData<>();
    public ObservableBoolean mLoginStatus = new ObservableBoolean(true);
    //    public ObservableField<Bitmap> mBitmap = new ObservableField<>();
    public ObservableField<Drawable> mDrawable = new ObservableField<>();
    private LoginButton mLoginButton;
    private AccessToken mAccessToken;
    private boolean mIsLoggedIn;
    private Activity mActivity;
    private CamScannerTask mCamScannerTask;

    public HomeViewModel() {
        mAccessToken = AccessToken.getCurrentAccessToken();

        mIsLoggedIn = mAccessToken != null && !mAccessToken.isExpired();

        if (mIsLoggedIn) {
            mLoginStatusText.setValue("Logged in!");
            mLoginStatus.set(false);
        } else {
            mLoginStatusText.setValue("Not logged in!");
            mLoginStatus.set(true);
        }
    }

    @BindingAdapter("android:src")
    public static void setImageDrawable(ImageView view, Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    public void setContext(Activity activity) {
        mActivity = activity;
        mCamScannerTask = CamScannerTask.getInstance(mActivity);
    }

    public void setLoginButton(LoginButton loginButton, CallbackManager callbackManager) {
        mLoginButton = loginButton;

        mLoginButton.setReadPermissions(Arrays.asList(EMAIL));

        mLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mAccessToken = loginResult.getAccessToken();
                mLoginStatusText.setValue("Logged in!");
                mLoginStatus.set(false);
                Toast.makeText(mActivity, "Login Success!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(mActivity, "Login Cancelled!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(mActivity, "Error signing in! " + error.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onFacebookLoginClicked() {
        if (!mIsLoggedIn) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity,
                    Arrays.asList("public_profile"));
        }
    }

    public void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");

        mActivity.startActivityForResult(intent, REQ_CODE_PICK_IMAGE);
    }

    public void processResult(int requestCode, int resultCode, Intent data) {
        mCamScannerTask.handleResult(requestCode, resultCode, data, new IImageResultCallback() {
            @Override
            public void getResultImage(Bitmap bitmap) {
                mDrawable.set(new BitmapDrawable(bitmap));
            }

            @Override
            public void onError(int errorCode) {
                Log.e(TAG, "" + errorCode);
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "Operation cancelled!");
            }
        });
    }

    public void openCamScanner(String path) {
        mCamScannerTask.openCamScanner(path);
    }

    public interface IImageResultCallback {
        void getResultImage(Bitmap bitmap);

        void onError(int errorCode);

        void onCancel();
    }
}
