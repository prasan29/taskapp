package app.task.com.taskapp.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;

import app.task.com.taskapp.R;
import app.task.com.taskapp.databinding.ActivityHomeBinding;
import app.task.com.taskapp.viewmodel.HomeViewModel;

public class HomeActivity extends AppCompatActivity {

    ActivityHomeBinding mBinding;
    private CallbackManager mCallbackManager;
    private HomeViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        FacebookSdk.fullyInitialize();

        mCallbackManager = CallbackManager.Factory.create();

        new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                if (currentProfile != null) {
                    Toast.makeText(HomeActivity.this, "Welcome, " +
                            currentProfile.getFirstName() + "!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(HomeActivity.this, "Logged out!", Toast.LENGTH_SHORT).show();
                    mViewModel.mLoginStatus.set(true);
                    mViewModel.mLoginStatusText.setValue("Not logged in!");
                }
            }
        };

        mViewModel.setContext(this);
        mViewModel.setLoginButton(mBinding.loginButton, mCallbackManager);
        mBinding.setLifecycleOwner(this);
        mBinding.setViewModel(mViewModel);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == mViewModel.REQ_CODE_CALL_CAMSCANNER) {
            mViewModel.processResult(requestCode, resultCode, data);
            Toast.makeText(this, "onActivityResult", Toast.LENGTH_SHORT).show();
        } else if (requestCode == mViewModel.REQ_CODE_PICK_IMAGE && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                Cursor cursor = getContentResolver().query(uri, new String[]{"_data"}, null, null,
                        null);
                if (cursor == null || !cursor.moveToNext()) {
                    return;
                }

                String path = cursor.getString(0);
                cursor.close();

                mViewModel.openCamScanner(path);
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

}
