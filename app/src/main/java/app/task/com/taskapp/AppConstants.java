package app.task.com.taskapp;

import android.os.Environment;

public final class AppConstants {
    public static final String API_KEY = "KLPt0gTtYUyP0fTXV8aH44e7";
    public static final String SCANNED_IMAGE = "scanned_img";
    public static final String SCANNED_PDF = "scanned_pdf";
    public static final String ORIGINAL_IMG = "ori_img";
    public static final String DIR_IMAGE =
            Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/TaskApp/images";
    public static final String OUTPUT_IMAGE_PATH = DIR_IMAGE + "/scanned.jpg";
    public static final String OUTPUT_PDF_PATH = DIR_IMAGE + "/scanned.pdf";
    public static final String OUTPUT_ORGINAL_PATH = DIR_IMAGE + "/original.jpg";
}
