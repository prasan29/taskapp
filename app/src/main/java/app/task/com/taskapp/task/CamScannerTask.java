package app.task.com.taskapp.task;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.intsig.csopen.sdk.CSOpenAPI;
import com.intsig.csopen.sdk.CSOpenAPIParam;
import com.intsig.csopen.sdk.CSOpenApiFactory;
import com.intsig.csopen.sdk.CSOpenApiHandler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import app.task.com.taskapp.AppConstants;
import app.task.com.taskapp.util.AppUtils;
import app.task.com.taskapp.viewmodel.HomeViewModel;

@SuppressLint("StaticFieldLeak")
public final class CamScannerTask {
    private static final int REQ_CODE_CALL_CAMSCANNER = 2;
    private static CamScannerTask mCamScanner;
    private CSOpenAPI mCSOpenAPI;
    private Activity mActivity;

    private CamScannerTask(Activity activity) {
        AppUtils.refreshFile(AppConstants.DIR_IMAGE);

        mActivity = activity;

        mCSOpenAPI = CSOpenApiFactory.createCSOpenApi(mActivity, AppConstants.API_KEY, "prasansrini29@gmail.com");
    }

    public static CamScannerTask getInstance(Activity activity) {
        if (mCamScanner == null) {
            mCamScanner = new CamScannerTask(activity);
        }

        return mCamScanner;
    }

    public void openCamScanner(String sourcePath) {
        try {
            FileOutputStream fos = new FileOutputStream(AppConstants.OUTPUT_ORGINAL_PATH);
            fos.write(3);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CSOpenAPIParam apiParam = new CSOpenAPIParam(sourcePath, AppConstants.OUTPUT_IMAGE_PATH,
                AppConstants.OUTPUT_PDF_PATH, AppConstants.OUTPUT_ORGINAL_PATH, 1.0F);

//        mCSOpenAPI = CSOpenApiFactory.createCSOpenApi(mActivity, AppConstants.API_KEY, null);

        mCSOpenAPI.scanImage(mActivity, REQ_CODE_CALL_CAMSCANNER, apiParam);
    }

    public void handleResult(int requestCode, int resultCode, Intent data,
                             final HomeViewModel.IImageResultCallback iImageResultCallback) {
        mCSOpenAPI.handleResult(requestCode, resultCode, data, new CSOpenApiHandler() {
            @Override
            public void onSuccess() {
                Toast.makeText(mActivity, "onSuccess", Toast.LENGTH_SHORT).show();
                iImageResultCallback.getResultImage(
                        AppUtils.getBitmap(AppConstants.OUTPUT_IMAGE_PATH));
            }

            @Override
            public void onError(int i) {
                Toast.makeText(mActivity, "onError", Toast.LENGTH_SHORT).show();
                iImageResultCallback.onError(i);
            }

            @Override
            public void onCancel() {
                Toast.makeText(mActivity, "onCancel", Toast.LENGTH_SHORT).show();
                iImageResultCallback.onCancel();
            }
        });
    }

}
